'use strict';

var ticketBooking = angular.module('ticketBooking', ['ngRoute'])
                        .config(function($routeProvider, $locationProvider, $httpProvider) {
                            $routeProvider.when('/', {
                                templateUrl: '/views/helpers/seats.html',
                                controller: 'mainController'
                            });

                            $locationProvider.html5Mode(true);
                        });

ticketBooking.controller('mainController', function($scope, $http, $timeout, $location) {
    $scope.seats = '';

    $http({
        method : 'post',
        url : '/seats'
    }).then(function(data) {
        $scope.seats = data.data;
        $('.loader').hide();
    }, function(err) {
        console.log(err);
    });

    $scope.selectSeat = function(event, seats) {
        if(seats.is_booked) {
            return false;
        }

        if($(event.target).hasClass('active')) {
            $(event.target).removeClass('active');
        } else {
            $(event.target).addClass('active');
        }

        var nb_seats = $('.seats.active').length, selected_seat_no = '';
        $('#nb_seats').html(nb_seats);

        for(var i = 0, len = $('.seats').length; i < len; i++)
        {
            if($('.seats').eq(i).hasClass('active'))
                selected_seat_no += i + ', ';
        }

        $('#selected_seats').html(selected_seat_no.replace(/,\s*$/, ''));
    }
});