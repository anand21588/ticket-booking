## Movie - Ticket Booking

### Steps to run on local
+ git clone git@gitlab.com:anand21588/ticket-booking.git
+ Open terminal, go to cloned directory run "npm install"
+ Run node server.js
+ Access localhost:3000 in your browser

## App deployed to heroku

+ https://ticket-booking-movie.herokuapp.com/
