var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    jsonFile = require('jsonfile'),
    request = require('request'),
    app = express();

app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/views', express.static(path.join(__dirname, 'views')));
app.use('/vendors', express.static(__dirname + 'node_modules'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/views/index.html');
});

app.post('/seats', function(req, res) {
    jsonFile.readFile('./public/seats.json', function(err, obj) {
        if(err) return err;
        
        return res.json(obj);
    });
});

app.listen(3000);
console.log('Port Connected in 3000');

exports = module.exports = app;
